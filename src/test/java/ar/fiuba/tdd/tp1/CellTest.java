package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by andres on 04/10/15.
 */
public class CellTest extends TestCase {

    private Spreadsheet spreadsheet;
    private Sheet sheet;
    private Cell celda;

    public CellTest() {
        spreadsheet = new Spreadsheet();
        sheet = new Sheet(spreadsheet);
        celda = new Cell(sheet,"A1");
    }

    @Test
    public void testCrearCelda() {
        assertNotNull(celda);
    }

    @Test
    public void testGetContenidoEsVacio() {
        assertTrue(celda.getValueAsString().contentEquals(""));
    }

    @Test
    public void testSetContenido() throws CircularReferenceException {
        celda.setValue("= 1 + 5");
        assertTrue(celda.getOriginalValue().contentEquals("= 1 + 5"));
    }

}