package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheetDriver;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by martin on 10/10/15.
 */
public class FunctionTests {

    private static final double DELTA = 0.0001;
    private SpreadSheetDriver testDriver;

    @Before
    public void setUp() throws CircularReferenceException {
        testDriver = new SpreadSheetDriver();
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1");
        testDriver.setCellValue("tecnicas", "default", "A2", "2");
        testDriver.setCellValue("tecnicas", "default", "A3", "3");
    }

    @Test
    public void maxLiterals() throws CircularReferenceException {
        testDriver.setCellValue("tecnicas", "default", "A4", "= MAX(A1:A3)");

        assertEquals(3, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
    }

    @Test
    public void minLiterals() throws CircularReferenceException {
        testDriver.setCellValue("tecnicas", "default", "A4", "= MIN(A1:A3)");

        assertEquals(1, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
    }

    @Test
    public void avgLiterals() throws CircularReferenceException {
        testDriver.setCellValue("tecnicas", "default", "A4", "= AVERAGE(A1:A3)");

        assertEquals(2, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
    }
}
