package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by martin on 3/10/15.
 */
public class SheetTest {
    private Spreadsheet spreadsheet;

    public SheetTest() {
        spreadsheet = new Spreadsheet();
    }

    @Test
    public void testCreateHoja() {
        Sheet sheet = new Sheet(spreadsheet);

        int posicionFila;
        int posicionCol;

        for (posicionFila = 65; posicionFila < 91; posicionFila++) {
            for (posicionCol = 1; posicionCol < 51; posicionCol++) {
                String key = Character.toString((char)posicionFila) + Integer.toString(posicionCol);
                assertTrue(sheet.getCell(key).getValueAsString().contentEquals(""));
            }
        }
    }

    @Test(expected = CircularReferenceException.class)
    public void testCycle() throws CircularReferenceException {
        Sheet sheet = new Sheet(spreadsheet);
        sheet.getCell("A1").setValue("= A2");
        sheet.getCell("A2").setValue("= A3");
        sheet.getCell("A3").setValue("= A1");
    }

    @Test(expected = CircularReferenceException.class)
    public void testCycleMultipleCells() throws CircularReferenceException {
        Sheet sheet = new Sheet(spreadsheet);
        sheet.getCell("A1").setValue("= A2 + A3");
        sheet.getCell("A2").setValue("= A3");
    }


}