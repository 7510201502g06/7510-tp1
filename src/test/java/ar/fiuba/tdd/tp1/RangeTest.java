package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.operations.Range;
import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by martin on 10/10/15.
 */
public class RangeTest {
    private Spreadsheet spreadsheet;

    public RangeTest() {
        spreadsheet = new Spreadsheet();
    }

    @Test
    public void testGetRange() {
        Range range = new Range("A1:C3",spreadsheet);

        List<String> cells = range.codeCellsInRange();
        String result = "";
        for (int position = 0; position < cells.size(); position++) {
            result += cells.get(position) + " ";
        }

        assertTrue("A1 A2 A3 B1 B2 B3 C1 C2 C3 ".equals(result));

    }

}
