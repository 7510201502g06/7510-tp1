package ar.fiuba.tdd.tp1;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by martin on 10/10/15.
 */
public class ExpressionParserTest {

    private ExpressionParser expressionParser;

    public ExpressionParserTest() {
        expressionParser = new ExpressionParser();
    }

    public String getStringFromList(List<String> parsedList) {
        String parsedExpression = "";
        for (int i = 0; i < parsedList.size(); i++) {
            parsedExpression += parsedList.get(i) + " ";
        }

        return parsedExpression;

    }

    @Test
    public void testExpresParser() {
        List<String> parsedList = expressionParser.parse("2 + 3 - 5 + 8");
        String parsedExpression = getStringFromList(parsedList);

        assertTrue(parsedExpression.contentEquals("2 3 + 5 - 8 + "));
    }

    @Test
    public void testExpresParserTwo() {
        List<String> parsedList = expressionParser.parse("2 + 3 - MAX(A1:D3) + 8");
        String parsedExpression = getStringFromList(parsedList);

        assertTrue(parsedExpression.contentEquals("2 3 + A1:D3 MAX - 8 + "));
    }

    @Test
    public void testExpresParserThree() {
        List<String> parsedList = expressionParser.parse("2 + 3 - CONCAT(A1,D3,B2) + 8");
        String parsedExpression = getStringFromList(parsedList);

        assertTrue(parsedExpression.contentEquals("2 3 + A1 D3 B2 CONCAT - 8 + "));
    }

    @Test
    public void testExpresParserFour() {
        List<String> parsedList = expressionParser.parse("MAX(A1:A3)");
        String parsedExpression = getStringFromList(parsedList);

        assertTrue(parsedExpression.contentEquals("A1:A3 MAX "));
    }

}
