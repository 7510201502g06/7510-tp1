package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by andres on 19/10/15.
 */
public class CSVDriverTest {

    private CSVDriver csvReader;
    private static final double DELTA = 0.0001;

    public CSVDriverTest() {
        csvReader = new CSVDriver();
    }

    @Test
    public void testCargarArchivoCSV() throws Exception {
        List<String[]> listRows = csvReader.leerArchivo("default.csv");
        assertEquals(4, listRows.size());
    }

    @Test
    public void testCargarSheetYPedirValorComoString() throws Exception {
        Spreadsheet spreadsheet = new Spreadsheet();
        Sheet sheetTest = csvReader.crearSheetDesdeCSV(spreadsheet,"default.csv");
        spreadsheet.addSheet("sheet1",sheetTest);
        String valorDeA2 = spreadsheet.getSheet("sheet1").getCell("A2").getOriginalValue();
        assertEquals("= 5 + 2", valorDeA2);
    }

    @Test
    public void testCargarWorksheetYPedirValorComoValue() throws Exception {
        Spreadsheet spreadsheet = new Spreadsheet();
        Sheet sheetTest = csvReader.crearSheetDesdeCSV(spreadsheet, "default.csv");
        spreadsheet.addSheet("sheet1",sheetTest);
        Double valorDeA2 = spreadsheet.getSheet("sheet1").getCell("A2").getValueAsDouble();
        assertEquals(7.0, valorDeA2, DELTA);
    }

    @Test
    public void testGuardarSheet() throws Exception {
        Spreadsheet spreadsheet = new Spreadsheet();
        Sheet sheet = csvReader.crearSheetDesdeCSV(spreadsheet, "default.csv");
        spreadsheet.addSheet("sheet1",sheet);
        csvReader.sheetaCSV(spreadsheet, "sheet1", "hoja1.csv");
        assertNotNull("hoja1.csv");
    }

    @Test
    public void testGuardarSpreadsheetYCargarloNuevamente() throws Exception {
        Spreadsheet spreadsheet = new Spreadsheet();
        Sheet sheet = csvReader.crearSheetDesdeCSV(spreadsheet, "default.csv");
        spreadsheet.addSheet("sheet1", sheet);
        csvReader.sheetaCSV(spreadsheet, "sheet1", "hoja1.csv");
        Spreadsheet spreadsheetLoader = new Spreadsheet();
        Sheet sheetRead = csvReader.crearSheetDesdeCSV(spreadsheetLoader, "hoja1.csv");
        spreadsheetLoader.addSheet("sheet1",sheetRead);
        assertEquals(spreadsheet.getSheet("sheet1").getCell("A1").getValueAsString(),
                spreadsheetLoader.getSheet("sheet1").getCell("A1").getValueAsString());

    }

    @Test(expected = FileNotFoundException.class)
    public void testFileNotFound() throws Exception {
        Spreadsheet spreadsheet = new Spreadsheet();
        csvReader.crearSheetDesdeCSV(spreadsheet, "archivo.csv");
    }

}