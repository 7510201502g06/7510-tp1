package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.operations.Operator;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by agustin on 04/10/15.
 */
public class ExpressionAdapter {

    TreeMap<Integer, TreeMap<String, String>> operators;


    public ExpressionAdapter() {
        this.operators = new TreeMap<>();
    }

    public void addOperators(List<Callable<Operator>> operadores) {
        for (Callable<Operator> operador : operadores) {
            this.addOperator(operador);
        }
    }

    public void addOperator(Callable<Operator> operador) {
        try {
            Operator operator = operador.call();
            this.operators.putIfAbsent(operator.getPrecedence(), new TreeMap<>());
            this.operators.get(operator.getPrecedence()).put(operator.getSymbol(), operator.getFunctionName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String adapt(String expression) {
        String operators;
        for (Map.Entry<Integer, TreeMap<String, String>> entry : this.operators.entrySet()) {
            TreeMap<String, String> someOperators = entry.getValue();
            operators = String.join("", someOperators.keySet());

            expression = expression.replace(" ", "");

            Pattern pattern = Pattern.compile("[" + operators + "]");
            Matcher matcher = pattern.matcher(expression);

            while (matcher.find()) {
                int parentheses = 0;
                int loop = getLoop(expression, matcher, parentheses);
                String left = expression.substring(loop, matcher.start());

                int segundoLoop = getSegundoLoop(expression, matcher, parentheses);
                String right = expression.substring(matcher.start() + 1, (segundoLoop));

                expression = expression.substring(0, loop) + someOperators.get(matcher.group())
                                + "(" + left + "," + right + ")" + expression.substring(segundoLoop);
                matcher = pattern.matcher(expression);
            }

        }

        return expression;

    }

    private int aperturaParentesisPrimerLoop(int parentesis) {
        if (parentesis == 0) {
            return -1;
        } else {
            return parentesis - 1;
        }
    }

    private int cerradoParentesisPrimerLoop(int parentesis) {
        return parentesis + 1;
    }

    private int ejecutarComa(int parentesis) {
        if (parentesis == 0) {
            return -1;
        } else {
            return parentesis;
        }
    }

    private int aperturaParentesisSegundoLoop(int parentesis) {
        return parentesis + 1;
    }

    private int cerradoParentesisSegundoLoop(int parentesis) {
        if (parentesis == 0) {
            return -1;
        } else {
            return parentesis - 1;
        }
    }

    private int getSegundoLoop(String expression, Matcher matcher, int parentheses) {
        int segundoLoop = matcher.start() + 1;
        while (segundoLoop < expression.length() && expression.substring(segundoLoop, segundoLoop + 1).matches("[HOJA1-9:A-Z0-9.(),]")) {
            String matchear = expression.substring(segundoLoop, segundoLoop + 1);
            parentheses = getParenthesesSegundoLoop(parentheses, matchear);
            if (parentheses < 0) {
                break;
            }
            segundoLoop++;
        }
        return segundoLoop;
    }

    private int getLoop(String expression, Matcher matcher, int parentheses) {
        int loop = matcher.start() - 1;
        while (loop >= 0 && expression.substring(loop, loop + 1).matches("[HOJA1-9:A-Z0-9.(),]")) {
            String matchear = expression.substring(loop, loop + 1);
            parentheses = getParenthesesPrimerLoop(parentheses, matchear);
            if (parentheses < 0) {
                break;
            }
            loop--;
        }
        loop++;
        return loop;
    }

    private int getParenthesesPrimerLoop(int parentheses, String matchear) {
        switch (matchear) {
            case "(":
                parentheses = aperturaParentesisPrimerLoop(parentheses);
                break;
            case ")":
                parentheses = cerradoParentesisPrimerLoop(parentheses);
                break;
            case ",":
                parentheses = ejecutarComa(parentheses);
                break;
            default:
        }
        return parentheses;
    }

    private int getParenthesesSegundoLoop(int parentheses, String matchear) {
        switch (matchear) {
            case "(":
                parentheses = aperturaParentesisSegundoLoop(parentheses);
                break;
            case ")":
                parentheses = cerradoParentesisSegundoLoop(parentheses);
                break;
            case ",":
                parentheses = ejecutarComa(parentheses);
                break;
            default:
        }
        return parentheses;
    }

}
