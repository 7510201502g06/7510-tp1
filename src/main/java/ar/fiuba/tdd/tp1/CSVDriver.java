package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;

import java.io.*;
import java.util.*;


/**
 * Created by andres on 16/10/15.
 */

public class CSVDriver {

    public List<String[]> leerArchivo(String inputFile) throws FileNotFoundException, FileHandlingException {

        BufferedReader fileReader = null;
        List<String[]> todasLasColumnas = new ArrayList<>();
        try {
            String line;
            fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), "UTF-8"));
            loadAllLines(fileReader, todasLasColumnas);
        } catch (IOException e) {
            throw new FileNotFoundException();
        } finally {
            closeFileReader(fileReader);
        }

        return todasLasColumnas;
    }

    private void loadAllLines(BufferedReader fileReader, List<String[]> todasLasColumnas) throws IOException {
        String line;
        while ((line = fileReader.readLine()) != null) {
            //Get all tokens available in line
            String[] tokens = line.split(",");
            todasLasColumnas.add(tokens);
        }
    }

    private void closeFileReader(BufferedReader fileReader) throws FileHandlingException {
        try {
            if (fileReader != null) {
                fileReader.close();
            }
        } catch (IOException e) {
            throw new FileHandlingException();
        }
    }

    public Sheet crearSheetDesdeCSV(Spreadsheet spreadsheet, String inputFile) throws FileNotFoundException,
            CircularReferenceException, FileHandlingException {
        List<String[]> csvParseado = leerArchivo(inputFile);
        Sheet newSheet = new Sheet(spreadsheet);
        for (String[] row : csvParseado) {
            newSheet.getCell(row[0]).setValue(row[1]);
        }

        return newSheet;
    }

    public void sheetaCSV(Spreadsheet spreadsheet, String sheet, String outputFile) throws FileHandlingException {
        HashMap<String, Sheet> aux = spreadsheet.getSpreadSheet();
        Writer writer;
        writer = initializeWriter(outputFile);
        Sheet sheetClass = aux.get(sheet);
        HashMap<String, Cell> celdas = sheetClass.getSheet();
        for (Map.Entry<String, Cell> celda : celdas.entrySet()) {
            String celdaString = celda.getKey();
            Cell celdaValue = celda.getValue();
            if (!celdaValue.getOriginalValue().equals("")) {
                String lineaAgregar = celdaString + "," + celdaValue.getOriginalValue() + "\n";
                addLineToFile(writer, lineaAgregar);
            }
        }
        closeFile(writer);
    }


    private void closeFile(Writer writer) throws FileHandlingException {
        try {
            if (writer != null) {
                writer.close();
            }
        } catch (IOException e) {
            throw new FileHandlingException();
        }
    }

    private void addLineToFile(Writer writer, String lineaAgregar) throws FileHandlingException {
        try {
            if (writer != null) {
                writer.append(lineaAgregar);
            }
        } catch (IOException e) {
            throw new FileHandlingException();
        }
    }

    private Writer initializeWriter(String outputFile) throws FileHandlingException {
        Writer writer;
        try {
            writer = new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8");
        } catch (IOException e) {
            throw new FileHandlingException();
        }
        return writer;
    }


}
