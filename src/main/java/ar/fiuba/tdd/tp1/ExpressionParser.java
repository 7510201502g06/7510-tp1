package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.operations.ExpressionRepository;
import ar.fiuba.tdd.tp1.operations.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.Callable;

/**
 * Created by martin on 9/10/15.
 */
public class ExpressionParser {

    private List<String> operators;

    public ExpressionParser() {
        ExpressionRepository expressionRepository = new ExpressionRepository();
        this.operators = new ArrayList<>();
        for (Callable<Operator> operatorCallable : expressionRepository.getOperators()) {
            try {
                this.operators.add(operatorCallable.call().getSymbol());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<String> parse(String expression) {
        String[] operators = expression.split(" ");
        ArrayList<String> parsedExpression = new ArrayList<>();
        Stack<String> operatorsTemporal = new Stack<>();
        int position = 0;
        String functionName;

        while (position < operators.length) {
            functionName = getFunctionName(operators[position]);
            if (!this.operators.contains(functionName)) {
                parsedExpression.add(operators[position]);
            } else {
                parseFunction(operators[position], parsedExpression, operatorsTemporal);
            }
            position++;
        }

        while (! operatorsTemporal.isEmpty()) {
            parsedExpression.add(operatorsTemporal.pop());
        }

        return parsedExpression;

    }

    private String getFunctionName(String expression) {
        int parenthesses = expression.indexOf("(");

        if (parenthesses == -1) {
            return expression;
        }

        String functionName = expression.substring(0, parenthesses);
        return functionName;
    }

    private void parseFunction(String function, List<String> parsedExpression , Stack<String> operatorsTemporal) {
        int parenthesses = function.indexOf("(");

        if (parenthesses == -1) {
            if (! operatorsTemporal.isEmpty()) {
                parsedExpression.add(operatorsTemporal.pop());
            }
            operatorsTemporal.push(function);
        } else {
            parseComplexFunction(function, parsedExpression, operatorsTemporal);
        }
    }

    private void parseComplexFunction(String function, List<String> parsedExpression , Stack<String> operatorsTemporal) {
        int parenthesses = function.indexOf("(");
        int comma = function.indexOf(",");

        if (comma == -1) { //One Argument Function
            parsedExpression.add(function.substring(parenthesses + 1, function.length() - 1));
            parsedExpression.add(function.substring(0, parenthesses));
            return;
        }

        parseFunctionWithManyArguments(function, parsedExpression);
    }

    private void parseFunctionWithManyArguments(String subExpression, List<String> parsedExpression) {
        int leftParenthesis = subExpression.indexOf("(");
        String function = subExpression.substring(0, leftParenthesis);
        String[] arguments = (subExpression.substring(leftParenthesis + 1, subExpression.length() - 1)).split(",");

        for (int position = 0; position < arguments.length; position++) {
            parsedExpression.addAll(parse(arguments[position]));
        }
        //parsedExpression.addAll(parse(arguments[0]));
        //parsedExpression.addAll(parse(arguments[1]));
        parsedExpression.add(function);
    }
}
