package ar.fiuba.tdd.tp1.spreadsheet.types;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormatException;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/**
 * Created by martin on 22/10/15.
 */
public class StringT implements Type {

    Cell cell;

    public StringT() {}

    @Override
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    @Override
    public void setFormat(String formatter, String format) {
        //A String Type is not settable
        throw new BadFormatException();
    }

    @Override
    public String getFormatted() {
        return cell.getOriginalValue();
    }
}

