package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.SpreadSheetCalculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Spreadsheet {

    private HashMap<String, Sheet> sheets;
    private SpreadSheetCalculator calculator;

    public Spreadsheet() {
        sheets = new HashMap<>();
        sheets.put("default", new Sheet(this));

        calculator = new SpreadSheetCalculator(this);

    }

    public SpreadSheetCalculator getCalculator() {
        return calculator;
    }

    public void createNewWorkSheet(String sheetName) {
        sheets.put(sheetName, new Sheet(this));
    }

    public List<String> workSheetsNames() {
        List<String> workSheetsNames = new ArrayList<>();
        for (String key : sheets.keySet()) {
            workSheetsNames.add(key);
        }

        return workSheetsNames;
    }


    public void removeWorkSheet(String nameWorkSheet) {
        this.sheets.remove(nameWorkSheet);
    }

    public Sheet getSheet(String workSheetName) {
        return sheets.get(workSheetName);
    }

    public void addSheet(String name, Sheet sheet) {
        sheets.put(name, sheet);
    }

    public int returnNumberOfSheets() {
        int cont = 0;
        for (String ignored : sheets.keySet()) {
            cont++;
        }
        return cont;
    }

    public boolean existsSheet(String sheetName) {
        return sheets.containsKey(sheetName);
    }

    public HashMap<String, Sheet> getSpreadSheet() {
        return sheets;
    }
}
