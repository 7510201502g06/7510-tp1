package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.FileHandlingException;
import org.json.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by agustin on 19/10/15.
 */
public class JsonDriver {

    public void toJson(Spreadsheet spreadsheet, String workBookName, String outputFile) throws FileHandlingException {
        HashMap<String, Sheet> aux = spreadsheet.getSpreadSheet();
        try {
            OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8");
            JSONWriter writer = new JSONWriter(fileWriter);
            writer = writer.object().key("name").value(workBookName)
                    .key("version").value("1.1").key("cells").array();

            for (Map.Entry<String, Sheet> entry : aux.entrySet()) {
                String sheet = entry.getKey();
                Sheet sheetClass = entry.getValue();
                HashMap<String, Cell> celdas = sheetClass.getSheet();
                for (Map.Entry<String, Cell> celda : celdas.entrySet()) {
                    if (celda.getValue().getValueAsString().compareTo("") != 0) {
                        writer = writer.object()
                                .key("sheet").value(sheet)
                                .key("id").value(celda.getKey())
                                .key("value").value(celda.getValue().getValueAsString())
                                .endObject();
                    }
                }
            }

            writer = writer.endArray().endObject();

            fileWriter.close();

        } catch (JSONException e) {
            System.out.println("EXCEPTION");
        } catch (IOException e) {
            throw new FileHandlingException();
        }

    }

    public void fromJson(String inputFile, SpreadSheetDriver spreadSheetDriver) throws FileHandlingException {

        try {
            InputStreamReader fileReader = new InputStreamReader(new FileInputStream(inputFile), "UTF-8");

            JSONTokener jsonTokener = new JSONTokener(fileReader);
            JSONObject mainObject = new JSONObject(jsonTokener);
            String workbookName = mainObject.getString("name");
            spreadSheetDriver.createNewWorkBookNamed(workbookName);

            JSONArray jsonCells = mainObject.getJSONArray("cells");
            for (int i = 0; i < jsonCells.length(); i++) {
                JSONObject jsonCell = jsonCells.getJSONObject(i);
                if (!spreadSheetDriver.workSheetNamesFor(workbookName).contains(jsonCell.getString("sheet")))    {
                    spreadSheetDriver.createNewWorkSheetNamed(workbookName, jsonCell.getString("sheet"));
                }
                String worksheetName = jsonCell.getString("sheet");
                String cellID = jsonCell.getString("id");
                String cellValue = jsonCell.getString("value");
                spreadSheetDriver.setCellValue(workbookName, worksheetName, cellID, cellValue);
            }

        } catch (JSONException e) {
            System.out.println("EXCEPTION");
        } catch (IOException e) {
            throw new FileHandlingException();
        }

    }

}
