package ar.fiuba.tdd.tp1.spreadsheet.types;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/**
 * Created by martin on 19/10/15.
 */
public class Number implements Type{

    int decimals;
    Cell cell;

    public Number() {
        this.decimals = 0;
    }

    @Override
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    @Override
    public void setFormat(String formatter, String format) {
        this.decimals = Integer.parseInt(format);
    }

    @Override
    public String getFormatted() {
        double value = cell.getValueAsDouble();
        StringBuffer strValue = new StringBuffer();
        strValue.append(String.valueOf(value));

        int positionDot = strValue.indexOf(".");

        int cantDecimals = strValue.length() - 1 - positionDot ;
        int position;

        // The number has no decimals
        if (positionDot == -1) {
            strValue.append(".");
            cantDecimals = 0;
        }

        if (cantDecimals < decimals) {
            for (position = 0; position < decimals - cantDecimals; position++) {
                strValue.append("0");
            }
        } else {
            position = positionDot + decimals;
            strValue.delete(position,strValue.length());
        }

        return strValue.toString();
    }
}
