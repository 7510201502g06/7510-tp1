package ar.fiuba.tdd.tp1.spreadsheet;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.cycle.PatonCycleBase;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import java.util.HashMap;


/**
 * Created by martin on 3/10/15.
 */
public class Sheet {
    private HashMap<String, Cell> celdas;
    private Spreadsheet spreadsheet;
    private UndirectedGraph<String, DefaultEdge> grafoSheet = new SimpleGraph<>(DefaultEdge.class);
    private PatonCycleBase<String,DefaultEdge> cycleAknowledge = new PatonCycleBase<>();

    public Spreadsheet getSpreadsheet() {
        return spreadsheet;
    }

    public Sheet(Spreadsheet spreadsheetParam) {
        spreadsheet = spreadsheetParam;
        celdas = new HashMap<>();

        int posicionFila;
        int posicionCol;

        for (posicionFila = 65; posicionFila < 91; posicionFila++) {
            for (posicionCol = 1; posicionCol < 51; posicionCol++) {
                String key = Character.toString((char) posicionFila) + Integer.toString(posicionCol);
                celdas.put(key, new Cell(this, key));
                grafoSheet.addVertex(key);
            }
        }
    }

    public void addVertex(String origin, String dest) {
        grafoSheet.addEdge(origin,dest);
    }

    public Cell getCell(String key) {
        return celdas.get(key);
    }

    public HashMap<String, Cell> getSheet() {
        return celdas;
    }

    public boolean isThereACycle() {
        cycleAknowledge.setGraph(grafoSheet);
        return (cycleAknowledge.findCycleBase().size() > 0);
    }
}
