package ar.fiuba.tdd.tp1.spreadsheet.types;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/**
 * Created by martin on 19/10/15.
 */
public class Money extends Number {

    String symbol;
    Cell cell;

    public Money() {
        symbol = "$";
    }

    @Override
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    @Override
    public void setFormat(String formatter, String format) {
        if (formatter.compareTo("decimal") == 0) {
            this.decimals = Integer.parseInt(format);
        } else {
            this.symbol = format;
        }
    }

    @Override
    public String getFormatted() {
        double value;
        try {
            value = cell.getValueAsDouble();
        } catch (Exception e) {
            return "Error:BAD_CURRENCY";
        }

        String formattedValue = String.format("%." + decimals + "f", value);
        formattedValue = symbol + " " + formattedValue;

        return formattedValue;
    }
}
