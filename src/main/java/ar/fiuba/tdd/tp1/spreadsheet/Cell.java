package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.*;
import ar.fiuba.tdd.tp1.acceptance.driver.BadFormatException;
import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.operations.FSString;
import ar.fiuba.tdd.tp1.spreadsheet.types.*;
import ar.fiuba.tdd.tp1.spreadsheet.types.Number;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by martin on 3/10/15.
 */
public class Cell implements IExpression {

    private String value;
    private String whatAmI;
    private Sheet sheet;
    private ExpressionParser expressionParser;
    private HashMap<String,Type> possibleTypes;
    private Type type;
    private String actualType;

    public Cell(Sheet sheetParameter, String cell) {
        value = "";
        sheet = sheetParameter;
        whatAmI = cell;
        type = null;
        actualType = "";
        possibleTypes = new HashMap<>();
        possibleTypes.put("Number", new Number());
        possibleTypes.put("Currency", new Money());
        possibleTypes.put("Date", new Date());
        possibleTypes.put("String", new StringT());
    }

    private boolean isFormula() {
        return value.startsWith("=");
    }

    private boolean isBadExpression(String valor) {
        return false;
        //return (valor.split(" ").length > 1);
    }

    public String getOriginalValue() {
        return value;
    }

    public IExpression getValueAsIExpression() {
        if (isFormula()) {
            expressionParser = new ExpressionParser();
            List<String> parsedExp = expressionParser.parse(value.substring(2));
            SpreadSheetCalculator calculator = sheet.getSpreadsheet().getCalculator();
            calculator.setCurrentSheet(sheet);

            IExpression returnValue;
            try {
                returnValue = calculator.calculate(parsedExp);
            } catch (NumberFormatException e) {
                throw new BadFormulaException();
            }
            return returnValue;
        } else  {
            try {
                return new ar.fiuba.tdd.tp1.operations.Number(Double.valueOf(value));
            } catch (NumberFormatException e) {
                return new FSString(value);
            }
        }
    }

    public String getValueAsString() {
        if (type != null) {
            return type.getFormatted();
        }

        return getValueAsIExpression().getValueAsString();
    }

    public double getValueAsDouble() {
        if (actualType.contentEquals("Date") || actualType.contentEquals("String")) {
            throw   new BadFormatException();
        }

        return getValueAsIExpression().getValueAsDouble();
    }

    public void setValue(String valueParam) throws CircularReferenceException {
        value = valueParam;
        List<String> cellsInvolver = cellsInvolved(valueParam);
        if (cellsInvolver.size() > 0) {
            for (String aux : cellsInvolver) {
                sheet.addVertex(whatAmI, aux);
            }
        }
        if (sheet.isThereACycle()) {
            throw new CircularReferenceException();
        }

    }

    private List<String> cellsInvolved(String valueParam) {
        List<String> allMatches = new ArrayList<String>();
        Matcher patron = Pattern.compile("[A-Z][1-9]").matcher(valueParam);
        while (patron.find()) {
            allMatches.add(patron.group());
        }
        return allMatches;
    }

    public void setType(String type) {
        this.type = possibleTypes.get(type);
        this.type.setCell(this);
        this.actualType = type;
    }

    public void setFormat(String formatter, String format) {
        type.setFormat(formatter, format);
    }

}
