package ar.fiuba.tdd.tp1.spreadsheet.commands;

import ar.fiuba.tdd.tp1.CircularReferenceException;
import ar.fiuba.tdd.tp1.FileHandlingException;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;

/**
 * Created by martin on 10/10/15.
 */
public class SetCellValueCommand implements Command {
    private Cell cell;
    private String value;
    private String oldValue;

    public SetCellValueCommand(Spreadsheet workBook, String workSheetName, String cellId, String value) {
        cell = workBook.getSheet(workSheetName).getCell(cellId);
        this.value = value;
        oldValue = cell.getOriginalValue();
    }

    @Override
    public void execute() throws CircularReferenceException {
        cell.setValue(value);
    }

    @Override
    public void undo() throws CircularReferenceException {
        cell.setValue(oldValue);
    }

    @Override
    public void redo() throws CircularReferenceException {
        cell.setValue(value);
    }

}
