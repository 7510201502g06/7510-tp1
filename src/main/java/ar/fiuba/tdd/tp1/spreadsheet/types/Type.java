package ar.fiuba.tdd.tp1.spreadsheet.types;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/**
 * Created by martin on 19/10/15.
 */
public interface Type {

    void setCell(Cell cell);

    void setFormat(String formatter, String format);

    String getFormatted();
}
