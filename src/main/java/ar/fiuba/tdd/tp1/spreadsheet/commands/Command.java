package ar.fiuba.tdd.tp1.spreadsheet.commands;

import ar.fiuba.tdd.tp1.CircularReferenceException;
import ar.fiuba.tdd.tp1.FileHandlingException;

/**
 * Created by martin on 10/10/15.
 */
public interface Command {
    public void execute() throws CircularReferenceException;

    public void undo() throws CircularReferenceException;

    public void redo() throws CircularReferenceException;
}
