package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.CSVDriver;
import ar.fiuba.tdd.tp1.CircularReferenceException;
import ar.fiuba.tdd.tp1.FileHandlingException;
import ar.fiuba.tdd.tp1.FileNotFoundException;
import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.spreadsheet.commands.Command;
import ar.fiuba.tdd.tp1.spreadsheet.commands.NewWorkSheetCommand;
import ar.fiuba.tdd.tp1.spreadsheet.commands.SetCellValueCommand;

import java.util.*;

/**
 * Created by martin on 9/10/15.
 */
public class SpreadSheetDriver implements SpreadSheetTestDriver {

    private Map<String, Spreadsheet> spreadSheets;
    private Stack<Command> commandStackUndos;
    private Stack<Command> commandStackRedos;

    public SpreadSheetDriver() {
        spreadSheets = new HashMap<>();
        commandStackUndos = new Stack<>();
        commandStackRedos = new Stack<>();
    }

    public List<String> workBooksNames() {
        List<String> workBooksNames = new ArrayList<>();
        for (String key : spreadSheets.keySet()) {
            workBooksNames.add(key);
        }

        return workBooksNames;
    }

    public void createNewWorkBookNamed(String name) {
        spreadSheets.put(name, new Spreadsheet());
    }

    public void createNewWorkSheetNamed(String workbookName, String name) {
        Spreadsheet workBook = spreadSheets.get(workbookName);
        Command command = new NewWorkSheetCommand(workBook, name);
        try {
            command.execute();
        } catch (CircularReferenceException e) {
            e.printStackTrace();
            return;
        }
        commandStackUndos.push(command);
        commandStackRedos.clear();
    }

    public List<String> workSheetNamesFor(String workBookName) {
        Spreadsheet workBook = spreadSheets.get(workBookName);

        return workBook.workSheetsNames();
    }

    public void setCellValue(String workBookName, String workSheetName, String cellId, String value)
            throws BadReferenceException {
        Spreadsheet workBook = spreadSheets.get(workBookName);
        Command command = new SetCellValueCommand(workBook, workSheetName, cellId, value);
        try {
            command.execute();
        } catch (CircularReferenceException e) {
            throw new BadReferenceException();
        }
        commandStackUndos.push(command);
        commandStackRedos.clear();
    }

    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        Spreadsheet workBook = spreadSheets.get(workBookName);
        Sheet workSheet = workBook.getSheet(workSheetName);
        Cell workCell = workSheet.getCell(cellId);

        return workCell.getValueAsString();
    }

    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {
        Spreadsheet workBook = spreadSheets.get(workBookName);
        Sheet workSheet = workBook.getSheet(workSheetName);
        Cell workCell = workSheet.getCell(cellId);

        return workCell.getValueAsDouble();
    }

    public void undo() throws BadReferenceException {
        if (commandStackUndos.isEmpty()) {
            return;
        }

        Command command = commandStackUndos.pop();
        try {
            command.undo();
        } catch (CircularReferenceException e) {
            throw new BadReferenceException();
        }
        commandStackRedos.push(command);
    }

    public void redo() throws BadReferenceException {
        if (commandStackRedos.isEmpty()) {
            return;
        }

        Command command = commandStackRedos.pop();
        try {
            command.redo();
        } catch (CircularReferenceException e) {
            throw new BadReferenceException();
        }
        commandStackUndos.push(command);

    }

    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format) {
        Spreadsheet workBook = spreadSheets.get(workBookName);
        Sheet workSheet = workBook.getSheet(workSheetName);
        Cell workCell = workSheet.getCell(cellId);

        workCell.setFormat(formatter, format);
    }

    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        Spreadsheet workBook = spreadSheets.get(workBookName);
        Sheet workSheet = workBook.getSheet(workSheetName);
        Cell workCell = workSheet.getCell(cellId);

        workCell.setType(type);
    }

    public void persistWorkBook(String workBookName, String fileName) {
        Spreadsheet workBook = spreadSheets.get(workBookName);
        JsonDriver jsonDriver = new JsonDriver();
        try {
            jsonDriver.toJson(workBook, workBookName, fileName);
        } catch (FileHandlingException e) {
            e.printStackTrace();
        }
    }

    public void loadFromCSV(String tecnicas, String sheet, String s1) {
        CSVDriver driver = new CSVDriver();
        Spreadsheet spreadsheet = new Spreadsheet();
        if (!spreadSheets.containsKey(tecnicas)) {
            spreadSheets.put(tecnicas, spreadsheet);
        }
        Sheet sheetTest = null;
        try {
            sheetTest = driver.crearSheetDesdeCSV(spreadsheet, s1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        spreadsheet.addSheet(sheet, sheetTest);
    }

    public void saveAsCSV(String tecnicas, String sheet, String s1) {
        Spreadsheet savedSheet = spreadSheets.get(tecnicas);
        CSVDriver driver = new CSVDriver();
        try {
            driver.sheetaCSV(savedSheet, sheet, s1);
        } catch (FileHandlingException e) {
            e.printStackTrace();
        }
        savedSheet.getSpreadSheet().clear();
        spreadSheets.remove(tecnicas);
    }

    public int sheetCountFor(String sheet) {
        Spreadsheet sheetCount = spreadSheets.get(sheet);
        return sheetCount.returnNumberOfSheets();
    }

    public void reloadPersistedWorkBook(String fileName) {
        JsonDriver jsonDriver = new JsonDriver();
        try {
            jsonDriver.fromJson(fileName, this);
        } catch (FileHandlingException e) {
            e.printStackTrace();
        }

    }
}
