package ar.fiuba.tdd.tp1.spreadsheet.types;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/**
 * Created by martin on 19/10/15.
 */
public class Date implements Type {

    private Cell cell;
    private String format;

    public Date() {
        format = "YYYY-MM-DD";
    }

    @Override
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    @Override
    public void setFormat(String formatter, String format) {
        this.format = format;
    }

    @Override
    public String getFormatted() {
        String date = cell.getOriginalValue();

        if (! isADate(date)) {
            return "Error:BAD_DATE";
        }

        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8,10);

        if (format.startsWith("DD")) {
            date = day + "-" + month + "-" + year;
        } else if (format.startsWith("MM")) {
            date = month + "-" + day + "-" + year;
        } else {
            date = year + "-" + month + "-" + day;
        }

        return date;
    }

    private boolean isADate(String date) {
        String dateRegex = "[0-9]{4}-[0-1][1-9]-[0-3][0-9]T[0-2][0-9]:[0-6][0-9]:[0-6][0-9]Z";

        if (date.matches(dateRegex)) {
            return true;
        }

        return false;
    }
}
