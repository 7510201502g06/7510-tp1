package ar.fiuba.tdd.tp1.spreadsheet.commands;

import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;

/**
 * Created by martin on 10/10/15.
 */
public class NewWorkSheetCommand implements Command {
    Spreadsheet spreadsheet;
    String nameWorkSheet;

    public NewWorkSheetCommand(Spreadsheet spreadsheet, String nameWorkSheet) {
        this.spreadsheet = spreadsheet;
        this.nameWorkSheet = nameWorkSheet;
    }

    @Override
    public void execute() {
        this.spreadsheet.createNewWorkSheet(this.nameWorkSheet);
    }

    @Override
    public void undo() {
        this.spreadsheet.removeWorkSheet(this.nameWorkSheet);
    }

    @Override
    public void redo() {
        this.spreadsheet.createNewWorkSheet(this.nameWorkSheet);
    }
}
