package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheetDriver;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by andres on 29/10/15.
 */
public class Repl {
    public static void main(String[] args) {

        SpreadSheetDriver driver = new SpreadSheetDriver();
        String workbook = "tecnicas";
        String sheet = "replSheet";
        driver.createNewWorkBookNamed(workbook);
        driver.createNewWorkSheetNamed(workbook,sheet);
        Scanner scanIn = new Scanner(System.in, "UTF-8");
        String accion = "";

        while (!accion.equals("quit")) {

            imprimirPantalla();
            String comando;

            comando = scanIn.nextLine();
            Matcher patron = Pattern.compile("\\(([^)]+)\\)").matcher(comando);
            accion = comando.split("\\(")[0];

            while (patron.find()) {
                String[] commandSheet = patron.group(1).split(",");
                getAccion(driver, accion, commandSheet);
            }
        }
        scanIn.close();
    }

    private static void getAccion(SpreadSheetDriver driver, String accion, String[] commandSheet) {
        String workbook = "tecnicas";
        String sheet = "replSheet";
        if (accion.equals("add")) {
            driver.setCellValue(workbook, sheet, commandSheet[0], commandSheet[1]);
        } else if (accion.equals("get")) {
            System.out.println(driver.getCellValueAsString(workbook, sheet, commandSheet[0]));
        } else if (accion.equals("type")) {
            driver.setCellType(workbook, sheet, commandSheet[0], commandSheet[1]);
            driver.setCellFormatter(workbook, sheet, commandSheet[0], commandSheet[2], commandSheet[3]);
        }
    }

    private static void imprimirPantalla() {
        System.out.println("| A | B | C | D | E | F | G | H | I | J | K | L | M | N | O | P | Q | R |"
                + " S | T | U | V | W | X | Y | Z |");
        System.out.println("");
        System.out.println("Introduzca un comando: ");
    }

}
