package ar.fiuba.tdd.tp1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;
/*
public class ExpressionBuilder {

    HashMap<String, Callable<Operator>> operations;

    public ExpressionBuilder() {
        this.operations = new HashMap<>();
    }

    public void addOperators(List<Callable<Operator>> operadores) {
        for (Callable<Operator> operador : operadores) {
            this.addOperation(operador);
        }
    }

    public void addOperation(Callable<Operator> operation) {
        String functionName = "";
        try {
            functionName = operation.call().getFunctionName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.operations.put(functionName, operation);
    }

    public Operator parse(Sheet sheet, String fullStringExpression) {

        int start = fullStringExpression.indexOf("(");
        if (start == -1 ) {
            return new Number(new Double(fullStringExpression));
        }
        int loop = start + 1;
        int segundoLoop = start + 1;
        int parentheses = 0;
        List<String> stringParameters = new ArrayList<>();
        List<IExpression> parameters = new ArrayList<>();
        while (segundoLoop < fullStringExpression.length()) {

            if (fullStringExpression.substring(segundoLoop, segundoLoop + 1).equals(",") && parentheses == 0) {
                stringParameters.add(fullStringExpression.substring(loop, segundoLoop));
                loop = segundoLoop;
            }

            parentheses = getParentheses(fullStringExpression, segundoLoop, parentheses);

            segundoLoop++;
        }
        stringParameters.add(fullStringExpression.substring(loop + 1, segundoLoop - 1));

        String numberRegex = "[0-9]+(.[0-9]+)?";
        String cellRegex = "(HOJA[1-9]:)?[A-Z]+[0-9]+";

        selectorExpresion(stringParameters, parameters, numberRegex, cellRegex, sheet);

        return getExpression(fullStringExpression, start, parameters);
    }

    private Operator getExpression(String fullStringExpression, int start, List<IExpression> parameters) {
        String functionName = fullStringExpression.substring(0, start);
        try {
            Operator operation = this.operations.get(functionName).call();
            operation.setOperators(parameters);
            return operation;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private int getParentheses(String fullStringExpression, int segundoLoop, int parentheses) {
        if (fullStringExpression.substring(segundoLoop, segundoLoop + 1).equals("(")) {
            parentheses++;
        }
        if (fullStringExpression.substring(segundoLoop, segundoLoop + 1).equals(")")) {
            parentheses--;
        }
        return parentheses;
    }

    private void selectorExpresion(List<String> stringParameters, List<IExpression> parameters, String numberRegex,
                                   String cellRegex, Sheet sheet) {
        for (String stringExpression : stringParameters) {
            if (stringExpression.matches(numberRegex)) {
                parameters.add(new Number(new Double(stringExpression)));
            } else if (stringExpression.matches(cellRegex)) {
                parameters.add(sheet.getCell(stringExpression));
            } else {
                parameters.add(this.parse(sheet, stringExpression));
            }
        }
    }

}
*/