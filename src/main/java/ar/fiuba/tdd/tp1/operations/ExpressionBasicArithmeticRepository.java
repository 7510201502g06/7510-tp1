package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IExpression;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by agustin on 11/10/15.
 */
public class ExpressionBasicArithmeticRepository {

    public static List<Callable<Operator>> getBasicArithmeticOperators() {
        List<Callable<Operator>> basicArithmeticOperators = new ArrayList<>();
        basicArithmeticOperators.add(() -> new Operator(
                        "+",
                        3,
                        "SUM",
                        (List<IExpression> operators) -> new Number(
                                operators.remove(0).getValueAsDouble() + operators.remove(0).getValueAsDouble()))
        );
        basicArithmeticOperators.add(() -> new Operator(
                        "-",
                        3,
                        "SUBSTRACT",
                        (List<IExpression> operators) -> new Number(
                                operators.remove(0).getValueAsDouble() - operators.remove(0).getValueAsDouble()))
        );
        return basicArithmeticOperators;
    }

}
