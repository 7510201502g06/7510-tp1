package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IExpression;

/**
 * Created by martin on 5/10/15.
 */
public class Number implements IExpression {

    private double op;

    public Number(double number) {
        op = number;
    }

    public String getValueAsString() {
        if (Math.floor(op) == op) {
            return String.valueOf((int)op);
        }
        return String.valueOf(op);
    }

    public double getValueAsDouble() {
        return op;
    }

    public IExpression getValueAsIExpression() {
        return this;
    }
}