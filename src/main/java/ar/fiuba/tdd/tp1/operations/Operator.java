package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IExpression;

import java.util.List;
import java.util.function.Function;

/**
 * Created by martin on 3/10/15.
 */
public class Operator implements IExpression {
    protected String symbol;
    protected int precedence;
    protected String functionName;
    protected Function<List<IExpression>, IExpression> operation;
    protected List<IExpression> operators;

    public Operator(String symbol, int precedence, String functionName, Function<List<IExpression>, IExpression> operation) {
        this.symbol = symbol;
        this.precedence = precedence;
        this.functionName = functionName;
        this.operation = operation;
    }

    public double getValueAsDouble() {
        return this.operation.apply(this.operators).getValueAsDouble();
    }

    public String getValueAsString() {
        return this.operation.apply(this.operators).getValueAsString();
    }

    public IExpression getValueAsIExpression() {
        return this.operation.apply(this.operators);
    }

    public String getSymbol() {
        return symbol;
    }

    public int getPrecedence() {
        return precedence;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setOperators(List<IExpression> operators) {
        this.operators = operators;
    }

}
