package ar.fiuba.tdd.tp1.operations;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class ExpressionRepository {
    private List<Callable<Operator>> operators;

    public ExpressionRepository() {
        this.operators = new ArrayList<>();
        this.operators.addAll(ExpressionBasicArithmeticRepository.getBasicArithmeticOperators());
        this.operators.addAll(ExpressionRangeFunctionsRepository.getRangeFunctionsOperators());
    }

    public List<Callable<Operator>> getOperators() {
        return operators;
    }
}
