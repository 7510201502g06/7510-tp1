package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IExpression;
import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;

/**
 * Created by martin on 5/10/15.
 */
public class FSString implements IExpression {

    private String op;

    public FSString(String string) {
        op = string;
    }

    public String getValueAsString() {
        return op;
    }

    public double getValueAsDouble() {
        throw new BadFormulaException();
    }

    public IExpression getValueAsIExpression() {
        return this;
    }
}