package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IExpression;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 10/10/15.
 */
public class Range implements IExpression {
    private String rangeCells;
    private Sheet sheet;

    public Range(String range, Spreadsheet spreadSheetReference) {
        String workSheet = "default";
        rangeCells = range;
        if (range.contains("!") ) {
            rangeCells = range.substring(1,range.indexOf("."));
            rangeCells = range.substring(range.indexOf(".") + 1);
        }

        sheet = spreadSheetReference.getSheet(workSheet);
    }

    @Override
    public double getValueAsDouble() {
        return 0;
    }

    public String getValueAsString() {
        return String.valueOf("");
    }

    public IExpression getValueAsIExpression() {
        return this;
    }

    public void setOperators(List<IExpression> operators) {

    }

    public List<Cell> getRange() {
        List<Cell> listFromRange = new ArrayList<>();
        List<String> codeCells = codeCellsInRange();

        int position;
        for (position = 0; position < codeCells.size(); position++) {
            String key = codeCells.get(position);
            listFromRange.add(sheet.getCell(key));
        }

        return listFromRange;
    }

    public List<String> codeCellsInRange() {
        String[] rangeLimits = rangeCells.split(":");
        List<String> codeCells = new ArrayList<>();

        int startRow = (int) (rangeLimits[0].charAt(0));
        int startColumn = Integer.parseInt(rangeLimits[0].substring(1, rangeLimits[0].length()));
        int endRow = (int) (rangeLimits[1].charAt(0));
        int endColumn = Integer.parseInt(rangeLimits[1].substring(1, rangeLimits[1].length()));

        for (int row = startRow; row <= endRow; row++) {
            for (int col = startColumn; col <= endColumn; col++) {
                String key = Character.toString((char)row) + Integer.toString(col);
                codeCells.add(key);
            }
        }

        return codeCells;
    }
}
