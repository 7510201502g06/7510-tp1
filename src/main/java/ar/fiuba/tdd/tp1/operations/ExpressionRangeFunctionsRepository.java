package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IExpression;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by agustin on 11/10/15.
 */
public class ExpressionRangeFunctionsRepository {

    public static List<Callable<Operator>> getRangeFunctionsOperators() {
        List<Callable<Operator>> operatorsList = new ArrayList<>();
        operatorsList.add(getMax());
        operatorsList.add(getMin());
        operatorsList.add(getAverage());
        operatorsList.add(getConcat());
        return operatorsList;
    }

    private static Callable<Operator> getMax() {
        return () -> new Operator(
                "MAX",
                3,
                "MAX",
                (List<IExpression> operators) -> {
                List<Cell> listOfRange = ((Range) operators.remove(0)).getRange();
                double max = listOfRange.get(0).getValueAsDouble();
                for (Cell cell : listOfRange) {
                    max = (cell.getValueAsDouble() > max) ? cell.getValueAsDouble() : max;
                }
                return new Number(max);
            });
    }

    private static Callable<Operator> getMin() {
        return () -> new Operator(
                "MIN",
                3,
                "MIN",
                (List<IExpression> operators) -> {
                List<Cell> listOfRange = ((Range) operators.remove(0)).getRange();
                double min = listOfRange.get(0).getValueAsDouble();
                for (Cell cell : listOfRange) {
                    min = (cell.getValueAsDouble() < min) ? cell.getValueAsDouble() : min;
                }
                return new Number(min);
            });
    }

    private static Callable<Operator> getAverage() {
        return () -> new Operator(
                "AVERAGE",
                3,
                "AVERAGE",
                (List<IExpression> operators) -> {
                Range range = (Range) operators.remove(0);
                List<Cell> listOfRange = range.getRange();

                double sum = 0;
                int totalElements = listOfRange.size();

                int position;
                for (position = 0; position < listOfRange.size(); position++) {
                    sum += listOfRange.get(position).getValueAsDouble();
                }

                return new Number(sum / totalElements);
            });
    }

    private static Callable<Operator> getConcat() {
        return () -> new Operator(
                "CONCAT",
                3,
                "CONCAT",
                (List<IExpression> operators) -> {

                String concatenatedString = new String("");
                int position;
                for (position = 0; position < operators.size(); position++) {
                    concatenatedString = concatenatedString.concat(operators.get(position).getValueAsString());
                }

                operators.clear();

                return new FSString(concatenatedString);
            });
    }

}
