package ar.fiuba.tdd.tp1;

import java.io.IOException;

/**
 * Created by andres on 19/10/15.
 */
public class CircularReferenceException extends IOException{
    private static final long serialVersionUID = 1L;
}
