package ar.fiuba.tdd.tp1;

public interface IExpression {

    double getValueAsDouble();

    String getValueAsString();

    IExpression getValueAsIExpression();
}
