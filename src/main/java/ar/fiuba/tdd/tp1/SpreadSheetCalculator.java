package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.operations.*;
import ar.fiuba.tdd.tp1.operations.Number;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import ar.fiuba.tdd.tp1.spreadsheet.Spreadsheet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by martin on 9/10/15.
 */
public class SpreadSheetCalculator {

    private HashMap<String, Callable<Operator>> operators;
    private Spreadsheet spreadsheet;
    private Sheet currentSheet;

    public SpreadSheetCalculator(Spreadsheet spreadsheetReference) {
        spreadsheet = spreadsheetReference;

        ExpressionRepository expressionRepository = new ExpressionRepository();
        this.operators = new HashMap<>();
        for (Callable<Operator> operatorCallable : expressionRepository.getOperators()) {
            try {
                this.operators.put(operatorCallable.call().getSymbol(), operatorCallable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Sheet currentSheet = null;
    }

    public IExpression calculate(List<String> operands) {
        List<IExpression> expression;
        try {
            expression = generateIExpression(operands);
        } catch (Exception e) {
            throw new BadFormulaException();
        }

        int position = 0;
        List<IExpression> actualOperands = new ArrayList<>();

        while (position < operands.size()) {
            if (!operators.containsKey(operands.get(position))) {
                actualOperands.add(expression.get(position));
                position++;
                continue;
            }

            Operator operator = (Operator) expression.get(position);
            operator.setOperators(actualOperands);
            actualOperands.add(operator.getValueAsIExpression());


            position++;
        }

        return actualOperands.get(0);
    }

    private List<IExpression> generateIExpression(List<String> operands) throws Exception {
        List<IExpression> expression = new ArrayList<>();

        for (int i = 0; i < operands.size(); i++) {
            String operand = operands.get(i);
            expression.add(getIExpression(operand));
        }

        return expression;
    }

    private IExpression getIExpression(String operand) throws Exception {
        String numberRegex = "-?[0-9]+(.[0-9]+)?";
        String cellRegex = "(![a-z]*.)?[A-Z]+[0-9]+";
        String rangeRegex = "[A-Z]+[0-9]+:[A-Z]+[0-9]+";

        if (operand.matches(numberRegex)) {
            return new Number(Double.parseDouble(operand));
        } else if (operand.matches(cellRegex)) {
            return getCell(operand);
        } else if (operand.matches(rangeRegex)) {
            return new Range(operand,spreadsheet);
        } else if (operators.containsKey(operand)) {
            return operators.get(operand).call();
        }

        throw new Exception();
    }

    private Cell getCell(String codeCell) {
        String workSheet = "default";

        String workCell = codeCell;
        if (codeCell.contains("!") ) {
            workSheet = codeCell.substring(1,codeCell.indexOf("."));
            workCell = codeCell.substring(codeCell.indexOf(".") + 1);
            return spreadsheet.getSheet(workSheet).getCell(workCell);
        }

        return currentSheet.getCell(workCell);
    }

    public void setCurrentSheet(Sheet sheet) {
        currentSheet = sheet;
    }
}
