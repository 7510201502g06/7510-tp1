package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.CircularReferenceException;
import ar.fiuba.tdd.tp1.FileHandlingException;
import ar.fiuba.tdd.tp1.FileNotFoundException;

import java.util.List;

public interface SpreadSheetTestDriver {

    List<String> workBooksNames();

    void createNewWorkBookNamed(String name);

    void createNewWorkSheetNamed(String workbookName, String name);

    List<String> workSheetNamesFor(String workBookName);

    void setCellValue(String workBookName, String workSheetName, String cellId, String value) throws BadReferenceException;

    String getCellValueAsString(String workBookName, String workSheetName, String cellId);

    double getCellValueAsDouble(String workBookName, String workSheetName, String cellId);

    void undo() throws BadReferenceException;

    void redo() throws BadReferenceException;

    void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format);

    void setCellType(String workBookName, String workSheetName, String cellId, String type);

    void persistWorkBook(String workBookName, String fileName);

    void reloadPersistedWorkBook(String fileName);

    void saveAsCSV(String tecnicas, String sheet, String s1);

    void loadFromCSV(String tecnicas, String sheet, String s1);

    int sheetCountFor(String tecnicas);
}
